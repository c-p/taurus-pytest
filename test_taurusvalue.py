"""
Reimplementation of taurus.qt.qtgui.panel.test.test_taurusvalue using pytest
"""


import pytest
from functools import partial
from taurus.external.qt import Qt
from taurus.qt.qtgui.display import TaurusLabel
from taurus.core.util.colors import ATTRIBUTE_QUALITY_DATA, DEVICE_STATE_DATA
from taurus.qt.qtgui.test.base import taurus_widget_set_models
from taurus.core.tango.test import taurus_test_ds  # pytest fixture


# TODO: move to another module where we parameterize taurus_widget_set_models
#  for a lot of widgets
test_models = partial(
    taurus_widget_set_models,
    klass=TaurusLabel,
    models=['sys/tg_test/1/wave', '', 'eval:1', None]
)


# colors:
_valid_color = ATTRIBUTE_QUALITY_DATA["ATTR_VALID"][1:4]
_ready_color = DEVICE_STATE_DATA["TaurusDevState.Ready"][1:4]
_transparent_color = Qt.QColor(Qt.Qt.transparent).getRgb()[:3]


@pytest.mark.parametrize(
    "attr_name,bgrole,expected",
    [
        ('float_scalar_ro', None, _valid_color),
        ('float_scalar_ro', 'quality', _valid_color),
        # ('float_scalar_ro', 'state', _ready_color),  # raises exception
        ('float_scalar_ro', 'none', _transparent_color),
    ]
)
def test_bg_role(qtbot, taurus_test_ds, attr_name, bgrole, expected):
    """Check the label background"""
    w = TaurusLabel()
    qtbot.addWidget(w)

    with qtbot.waitSignal(w.taurusEvent, timeout=3200):
        w.setModel("tango:{}/{}".format(taurus_test_ds, attr_name))
        if bgrole is not None:
            w.setBgRole(bgrole)

    p = w.palette()
    got = p.color(p.Background).getRgb()[:3]  # RGB value of the background
    assert got == expected
