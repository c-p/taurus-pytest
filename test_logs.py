import taurus
from taurus.core.util.log import deprecation_decorator
import contextlib
import functools
import pytest


def foo():
    taurus.warning('hello')


@deprecation_decorator(alt="bar", rel="1.2.3")
def bar():
    taurus.warning('bye')


# Example on how to test using pytest and caplog directly

def test_foo1(caplog):
    """check deprecations in foo()"""
    foo()
    for record in caplog.records:
        assert "DeprecationWarning" not in record.msg

@pytest.mark.xfail
def test_bar1(caplog):
    """check deprecations in bar()"""
    bar()
    for record in caplog.records:
        assert "DeprecationWarning" not in record.msg


# How to check deprecations using a context manager

@contextlib.contextmanager
def check_taurus_deprecations(caplog, expected=0):
    """context manager that checks the number of taurus deprecation warnings
    logged within the context execution.

    :param expected: (int) Expected number of deprecations. Default is 0
    """
    from taurus import tauruscustomsettings
    # disable deprecation silencing in this context
    bck = getattr(tauruscustomsettings, "MAX_DEPRECATIONS_LOGGED", None)
    tauruscustomsettings._MAX_DEPRECATIONS_LOGGED = None
    caplog.clear()
    try:
        yield
    finally:
        tauruscustomsettings._MAX_DEPRECATIONS_LOGGED = bck
    # check the deprecations after the context is run
    deps = [r for r in caplog.records if "DeprecationWarning" in r.msg]
    n = len(deps)
    msg = "{} Deprecation Warnings ({} expected)".format(n, expected)
    assert len(deps) == expected, msg


def test_foo2(caplog):
    with check_taurus_deprecations(caplog):
        foo()


def test_bar2(caplog):
    with check_taurus_deprecations(caplog, expected=1):
        bar()


# How to test using an optionally-parameterizable decorator

def taurus_deprecations_check(func=None, expected=0):
    """Decorator that adds a check on the number of taurus deprecation warnings
    logged during the execution of the decorated function.

    :param expected: (int) Expected number of deprecations. Default is 0
    """
    if func is None:
        return functools.partial(taurus_deprecations_check, expected=expected)

    # TODO how to usee functools.wraps when inserting an extra arg? (caplog)
    # @functools.wraps(func)
    def wrapper(caplog, *args, **kwargs):
        with check_taurus_deprecations(caplog, expected=expected):
            ret = func(*args, **kwargs)
        return ret
    # Since functools.wraps cannot be used, manually set the name and docstring
    wrapper.__name__ = func.__name__
    wrapper.__doc__ = func.__doc__
    return wrapper


@taurus_deprecations_check
def test_foo3():
    foo()


@taurus_deprecations_check()
def test_foo4():
    foo()


@pytest.mark.xfail
@taurus_deprecations_check
def test_bar3():
    bar()


@taurus_deprecations_check(expected=1)
def test_bar4():
    bar()


@pytest.mark.xfail
@taurus_deprecations_check
def test_baz():
    assert 0, "THIS WAS ON PURPOSE!"


@pytest.mark.xfail
@taurus_deprecations_check
def test_zab():
    bar()  # <-- this logs a deprecation!
    assert 0, "THIS WAS ON PURPOSE!"

