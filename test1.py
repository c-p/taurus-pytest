from taurus.qt.qtgui.display import TaurusLabel
from taurus.core.util.colors import ATTRIBUTE_QUALITY_DATA


def test_bgRole(qtbot):
    """Check the label text"""
    model= "sys/tg_test/1/short_scalar"
    expected = ATTRIBUTE_QUALITY_DATA["ATTR_VALID"][1:4]
    w = TaurusLabel()
    qtbot.addWidget(w)

    # from taurus.external.qt import Qt
    # w.setModel(model)
    # app = Qt.QApplication.instance()
    # import time
    # for i in range(50):
    #     time.sleep(.1)
    #     app.processEvents()

    with qtbot.waitSignal(w.taurusEvent, timeout=3200):
        w.setModel(model)

    p = w.palette()
    got = p.color(p.Background).getRgb()[:3]  # RGB value of the background
    assert got == expected